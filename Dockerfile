FROM python:3.11

WORKDIR /app

ADD requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

RUN chown -R 1000:1000 .
USER 1000:1000

ADD main.py main.py

CMD ["python3", "main.py"]