from kafka import KafkaConsumer, KafkaProducer
from joblib import load
import os
import json

inputTopic = os.environ['KAFKA_INPUT_ANOMALY_TOPIC']
outputTopic = os.environ['KAFKA_OUTPUT_ANOMALY_TOPIC']
kafkaGroupId = os.environ['KAFKA_GROUP_ID']
kafkaBrokers = os.environ['KAFKA_BROKERS'].split(',')
kafkaSecurityProtocol = os.environ['KAFKA_SECURITY_PROTOCOL']
kafkaCaFile = os.environ['KAFKA_CA_FILE']
modelsDir = os.environ['MODELS_DIR']


def parseSensorData(_json):
    data = json.loads(_json)
    return list(map(lambda x: 0 if len(x) == 0 else x[0], data.values()))


print("Starting consumer...")
producer = KafkaProducer(
    bootstrap_servers=kafkaBrokers,
    security_protocol=kafkaSecurityProtocol,
    ssl_cafile=kafkaCaFile,
)

consumer = KafkaConsumer(
    inputTopic,
    group_id=kafkaGroupId,
    bootstrap_servers=kafkaBrokers,
    auto_offset_reset='earliest',
    enable_auto_commit=True,
    security_protocol=kafkaSecurityProtocol,
    ssl_cafile=kafkaCaFile,
)

for message in consumer:
    print(f"Consumed offset: {message.offset}...")

    key = message.key.decode()
    value = json.loads(message.value)

    # TODO: we could keep clfs loaded between messages.
    anomalyModelUuid = value['anomaly_model_uuid']
    print(f"Loading model {anomalyModelUuid}...")
    clf = load(f"{modelsDir}/{anomalyModelUuid}.joblib")

    print(f"Predicting...")
    sensorValue = parseSensorData(value['json'])
    isAnomaly = clf.predict([sensorValue])[0] == 1

    if not isAnomaly:
        continue

    print("Anomaly detected, producing")
    producer.send(
        outputTopic,
        json.dumps({
            'deviceUuid': key,
            'date': value['date']
        }).encode()
    )
    producer.flush()
